#!/usr/bin/env python
#  genDepMap.py
# https://stackoverflow.com/questions/14242295/build-a-dependency-graph-in-python
# https://ocefpaf.github.io/python4oceanographers/blog/2014/11/17/networkX/

import networkx as nx
import re

def genTargetPath(raw):
    regex = re.compile(r'^\s*([A-Za-z][A-Za-z0-9_\-]+)\s*:\s*([A-Za-z][A-Za-z0-9_\-]+)\s*$')

    Graw = nx.DiGraph()
    for l in raw.splitlines():
        if len(l):
            m = regex.match(l)
            if m:
                target, prereq = m.groups()
                Graw.add_edge(prereq, target)

    allnodes = {n:n for n in Graw.nodes}
    leaves = dict(allnodes)
    roots  = dict(allnodes)
    comps  = dict(allnodes)
    for prereq, target in Graw.edges():
        if leaves.has_key(prereq): del(leaves[prereq])
        if roots.has_key(target): del(roots[target])
    for prereq, target in Graw.edges():
        if comps.has_key(prereq) and target not in leaves.keys():
            del(comps[prereq])

    G2 = Graw.reverse()

    G = G2.copy()
    paths = []
    dbg = True
    for s in leaves:
        if dbg: print s
        steps = []
        steps.append(s)
        spacer = {s: 0}
        for prereq, target in nx.dfs_edges(G, s):
            spacer[target] = spacer[prereq] + 2
            if dbg: print '{spacer}+-{t}'.format(
                                     spacer=' ' * spacer[prereq],
                                     t=target)
            steps.append(target)
        if dbg: print ''
        paths.append(steps)
    for e in paths:
        print e

raw = '''
# dependencies in the format of " prerequisite : target " 

Toolchain : Reposync

Kernel : Toolchain

Rootfs : Toolchain
Rootfs : Kernel

App1 : Reposync
App1 : Toolchain

App2 : Reposync
App2 : Toolchain

Release : Kernel
Release : Rootfs
Release : App1
Release : App2
'''

genTargetPath(raw)

''' the output tree is right but not the steps

Release
+-Kernel
  +-Toolchain
    +-Reposync
+-App2
+-App1
+-Rootfs

['Release', 'Kernel', 'Toolchain', 'Reposync', 'App2', 'App1', 'Rootfs']
'''
