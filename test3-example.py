#!/usr/bin/env python
#  test3-example.py
# https://stackoverflow.com/questions/14242295/build-a-dependency-graph-in-python
# https://ocefpaf.github.io/python4oceanographers/blog/2014/11/17/networkX/

import networkx as nx
import re

def test1(raw):
    regex = re.compile(r'^([A-Z]+)::Requires\s+=\s([A-Z"]+)$')

    Graw = nx.DiGraph()
    roots = set()
    for l in raw.splitlines():
        if len(l):
            target, prereq = regex.match(l).groups()
            if prereq == '""':
                roots.add(target)
            else:
                Graw.add_edge(prereq, target)

    allnodes = {n:n for n in Graw.nodes}
    leaves = dict(allnodes)
    for prereq, target in Graw.edges():
        if leaves.has_key(prereq):
            del(leaves[prereq])

    G = Graw.reverse()

    for s in leaves:
        print s
        spacer = {s: 0}
        for prereq, target in nx.dfs_edges(G, s):
            spacer[target] = spacer[prereq] + 2
            print '{spacer}+-{t}'.format(
                                         spacer=' ' * spacer[prereq],
                                         t=target)
        print ''

raw = '''
A::Requires         = ""
B::Requires     = A
C::Requires     = B
H::Requires     = A

AA::Requires         = ""
BB::Requires         = AA
CC::Requires     = B

CC::Requires    = BB
'''

test1(raw)

'''test2 prints
C
+-B
  +-A

CC
+-B
  +-A
+-BB
  +-AA

H
+-A

 test2 prints:
A
+-H
+-B
  +-CC
  +-C

AA
+-BB
  +-CC
'''
