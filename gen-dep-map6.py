#!/usr/bin/env python
#  genDepMapSteps.py
# https://stackoverflow.com/questions/14242295/build-a-dependency-graph-in-python
# https://ocefpaf.github.io/python4oceanographers/blog/2014/11/17/networkX/

import networkx as nx
import re

def genTargetPath(raw):
    regex = re.compile(r'^\s*([A-Za-z][A-Za-z0-9_\-]+)\s*:\s*([A-Za-z][A-Za-z0-9_\-]+)\s*$')

    Graw = nx.DiGraph()
    for l in raw.splitlines():
        if len(l):
            m = regex.match(l)
            if m:
                target, prereq = m.groups()
                Graw.add_edge(prereq, target)

    allnodes = {n:n for n in Graw.nodes}
    leaves = dict(allnodes)
    roots  = dict(allnodes)
    comps  = dict(allnodes)
    for prereq, target in Graw.edges():
        if leaves.has_key(prereq): del(leaves[prereq])
        if roots.has_key(target): del(roots[target])
    for prereq, target in Graw.edges():
        if comps.has_key(prereq) and target not in leaves.keys():
            del(comps[prereq])

    G2 = Graw.reverse()

    def trackSteps():
        G = G2.copy()
        actions = []
        dbg = True
        for s in leaves:
            if dbg: print s
            spacer = [{s: 0}]
            def trackNode(node, thepath, spacer):
                for prereq, target in nx.dfs_edges(G, node, 1):
                    if target not in thepath:
                        spacer[0][target] = spacer[0][prereq] + 2
                        if dbg: print '{spacer}+-{t}'.format(
                                     spacer=' ' * spacer[0][prereq],
                                     t=target)
                        trackNode(target, thepath, spacer)
                if node not in thepath:
                    thepath.append(node)
            trackNode(s, actions, spacer)
            if dbg: print ''
        return actions

    steps = trackSteps()
    print steps

raw = '''
# dependencies in the format of " prerequisite : target " 

Toolchain : Reposync

Kernel : Toolchain

Rootfs : Toolchain
Rootfs : Kernel

App1 : Reposync
App1 : Toolchain

App2 : Reposync
App2 : Toolchain

Release : Kernel
Release : Rootfs
Release : App1
Release : App2
'''

genTargetPath(raw)

''' the output tree is right and also the steps

Release
+-Kernel
  +-Toolchain
    +-Reposync
+-App2
+-App1
+-Rootfs

['Reposync', 'Toolchain', 'Kernel', 'App2', 'App1', 'Rootfs', 'Release']
'''
