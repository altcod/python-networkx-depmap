#!/usr/bin/env python
#  test1-example.py
# https://stackoverflow.com/questions/14242295/build-a-dependency-graph-in-python
# https://ocefpaf.github.io/python4oceanographers/blog/2014/11/17/networkX/

import networkx as nx
import re

def test1(raw):
    regex = re.compile(r'^([A-Z]+)::Requires\s+=\s([A-Z"]+)$')

    G = nx.DiGraph()
    roots = set()
    for l in raw.splitlines():
        if len(l):
            target, prereq = regex.match(l).groups()
            if prereq == '""':
                roots.add(target)
            else:
                G.add_edge(prereq, target)

    for s in roots:
        print s
        spacer = {s: 0}
        for prereq, target in nx.dfs_edges(G, s):
            spacer[target] = spacer[prereq] + 2
            print '{spacer}+-{t}'.format(
                                         spacer=' ' * spacer[prereq],
                                         t=target)
        print ''

raw = '''
A::Requires         = ""
B::Requires     = A
C::Requires     = B
H::Requires     = A

AA::Requires         = ""
BB::Requires         = AA
C::Requires     = B

CC::Requires    = BB
'''

test1(raw)

''' this prints: 
A
+-H
+-B
  +-C

AA
+-BB
  +-CC
'''
